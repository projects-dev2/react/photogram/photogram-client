import React, {useState} from 'react';
import {Container, Image} from 'semantic-ui-react';

import logo from '../../assets/png/logo.png';
import {RegisterForm} from "../../components/Auth/RegisterForm";
import {LoginForm} from "../../components/Auth/LoginForm";

export const AuthScreen = () => {

    const [showLogin, setShowLogin] = useState(true);

    const handleRegister = () => {
        setShowLogin(!showLogin);
    }

    const handleLogin = () => {
        setShowLogin(!showLogin);
    }

    return (

        <Container fluid className="auth__container">
            <Image src={logo}/>

            <div className="auth__form">

                {
                    showLogin ?
                        <div className="auth_login">
                            <LoginForm />
                        </div>

                        :

                        <div>
                            <RegisterForm setShowLogin={setShowLogin}/>
                        </div>
                }

            </div>

            <div className="auth_change-form">

                {
                    showLogin ?

                        (
                            <>Don't have an account?
                                <span onClick={handleRegister}>Sign up</span>
                            </>
                        )
                        :
                        (
                            <>Have an account?
                                <span onClick={handleLogin}>Log in</span>
                            </>
                        )
                }

            </div>

        </Container>

    )
};