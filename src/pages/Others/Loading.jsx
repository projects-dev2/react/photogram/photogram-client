import React, {useEffect} from 'react';

export const Loading = ({setLoad}) => {

    useEffect(() => {

        setTimeout(() => {
            setLoad(data => !data);
        }, 1250);
    }, [setLoad]);

    return (

        <div className="loading__corp-three">

            <div className="loading__loadingFive">
                <span> </span>
                <span> </span>
                <span> </span>
                <span> </span>
            </div>

        </div>

    )
};