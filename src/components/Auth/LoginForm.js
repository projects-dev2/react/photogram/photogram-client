import React from 'react';
import {Form, Button, Message} from 'semantic-ui-react';

import * as Yup from 'yup';
import {toast} from "react-toastify";

import {decodeToken, setToken} from "../../utils/token";
import {LOGIN} from "../../gql/user";

import {useFormik} from "formik";
import {useMutation} from "@apollo/client";
import {useAuth} from "../../hooks/useAuth";

export const LoginForm = () => {

        const [login] = useMutation(LOGIN);
        const {setUser} = useAuth();

        const formik = useFormik({

            initialValues: {
                email: '',
                password: '',
            },

            validationSchema: Yup.object({
                email: Yup.string().email('This email is invalid')
                    .required("The email is required"),
                password: Yup.string().required('The password is required')
            }),

            onSubmit: async (formData) => {

                try {
                    const {data} = await login({
                        variables: {
                            input: formData
                        }
                    });

                    const {token} = data.login;
                    toast.success('The login was correct')
                    setToken(token);
                    setUser(decodeToken(token));

                } catch (error) {
                    toast.error(error.message);
                }

            },
        });

        return (

            <Form className="auth__login-form" onSubmit={formik.handleSubmit}>

                <h3 className="auth__title-login">Photos and videos of your friends</h3>

                <Form.Input
                    type="text" placeholder="Write your email"
                    name="email" {...formik.getFieldProps('email')}
                />

                {formik.touched.email && formik.errors.email ?
                    (
                        <Message negative>
                            <p className="auth__message-alert">{formik.errors.email}</p>
                        </Message>
                    ) : null
                }

                <Form.Input
                    type="password" placeholder="Write your password"
                    name="password" {...formik.getFieldProps('password')}
                />

                {formik.touched.password && formik.errors.password ?
                    (
                        <Message negative>
                            <p className="auth__message-alert">{formik.errors.password}</p>
                        </Message>
                    ) : null
                }

                <Button type="submit" className="btn-submit">Log in</Button>

            </Form>

        )
    }
;