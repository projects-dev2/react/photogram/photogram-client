import React from 'react';
import {Form, Button, Message} from 'semantic-ui-react';

import * as Yup from 'yup';
import {useFormik} from "formik";

import {useMutation} from '@apollo/client';
import {REGISTER} from '../../gql/user';

import {toast} from "react-toastify";

export const RegisterForm = ({setShowLogin}) => {

    const [register] = useMutation(REGISTER);

    const formik = useFormik({

        initialValues: {
            name: '',
            username: '',
            email: '',
            password: '',
            repeatPassword: '',
        },

        validationSchema: Yup.object({
            name: Yup.string().required("The name is required"),
            username: Yup.string()
                .matches(/^[a-zA-Z0-9-]*$/, "Your username can't have space")
                .required("The Username is required"),
            email: Yup.string().email('This email is invalid')
                .required("The email is required"),
            password: Yup.string().required('The password is required')
                .oneOf([Yup.ref('repeatPassword')], 'Passwords are not the same'),
            repeatPassword: Yup.string().required('The password is required')
                .oneOf([Yup.ref('password')], 'Passwords are not the same')
        }),

        onSubmit: async (formData) => {

            try {
                const newUser = formData;
                delete newUser.repeatPassword;

                await register({
                    variables: {
                        input: newUser
                    },
                });

                toast.success('User successfully registered');
                setShowLogin( value =>  !value);

            } catch (error) {
                toast.error(error.message);
            }
        },
    });

    return (

        <Form className="auth__register-form" onSubmit={formik.handleSubmit}>

            <h2 className="auth__register-form-title">Register in the best social network </h2>

            <Form.Input
                type="text" placeholder="Full Name" name="name"
                autoComplete="nope" {...formik.getFieldProps('name')}
            />

            {formik.touched.name && formik.errors.name ?
                (
                    <Message negative>
                        <p className="auth__message-alert">{formik.errors.name}</p>
                    </Message>
                ) : null
            }

            <Form.Input
                type="text" placeholder="UserName" name="username"
                autoComplete="nope" {...formik.getFieldProps('username')}
            />

            {formik.touched.username && formik.errors.username ?
                (
                    <Message negative>
                        <p className="auth__message-alert">{formik.errors.username}</p>
                    </Message>
                ) : null
            }

            <Form.Input
                type="text" placeholder="Email" autoComplete="nope"
                name="email" {...formik.getFieldProps('email')}
            />

            {formik.touched.email && formik.errors.email ?
                (
                    <Message negative>
                        <p className="auth__message-alert">{formik.errors.email}</p>
                    </Message>
                ) : null
            }

            <Form.Input
                type="password" placeholder="Password"
                name="password" {...formik.getFieldProps('password')}
            />

            {formik.touched.password && formik.errors.password ?
                (
                    <Message negative>
                        <p className="auth__message-alert">{formik.errors.password}</p>
                    </Message>
                ) : null
            }

            <Form.Input
                type="password" placeholder="Confirm password"
                name="repeatPassword" {...formik.getFieldProps('repeatPassword')}
            />

            {formik.touched.repeatPassword && formik.errors.repeatPassword ?
                (
                    <Message negative>
                        <p className="auth__message-alert">{formik.errors.repeatPassword}</p>
                    </Message>
                ) : null
            }

            <Button type="submit" className="btn-submit">Register</Button>
            {/*<Button type="button" onClick={formik.handleReset}>Reset Form</Button>*/}

        </Form>

    )
};

// function initialValues() {
//     return {
//         name: '',
//         username: '',
//         email: '',
//         password: '',
//         repeatPassword: '',
//     }
// }
