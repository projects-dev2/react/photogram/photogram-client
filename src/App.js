import {ApolloProvider} from '@apollo/client'
import client from './config/client';
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import {AuthScreen} from "./pages/Auth/AuthScreen";
import {useEffect, useMemo, useState} from "react";
import {getToken} from "./utils/token";
import {Loading} from "./pages/Others/Loading";
import {Home} from "./pages/Home/Home";

import AuthContext from "./context/AuthContext";

function App() {

    const [auth, setAuth] = useState(undefined);
    const [load, setLoad] = useState(false);

    useEffect(() => {

        const token = getToken();
        if (!token) {
            setAuth(null);
        } else {
            setAuth(token);
        }

    }, []);

    const logout = () => {
        console.log('logout');
    }

    const setUser = (user) => {
        setAuth(user);
    }

    const authData = useMemo(() => ({auth, logout, setUser}), [auth]);

    if (!load) {
        return <Loading setLoad={setLoad}/>
    }

    return (

        <ApolloProvider client={client}>

            <AuthContext.Provider value={authData}>

                {!auth ? <AuthScreen/> : <Home/>}

                <ToastContainer
                    position="top-right" autoClose={6500}
                    newestOnTop closeOnClick
                    rtl={false} pauseOnFocusLoss
                    draggable pauseOnHover
                />


            </AuthContext.Provider>

        </ApolloProvider>
    );
}

export default App;